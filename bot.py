from telethon import TelegramClient
import logging, asyncio
from telethon.tl.types import MessageMediaDocument, DocumentAttributeSticker, DocumentAttributeAnimated, MessageMediaPhoto
from telethon import events
from telethon.tl.custom import Button
from os.path import isfile, join
from os import system
from time import time
from PIL import Image, ImageFont
from config import *
from os import makedirs
import subprocess

logging.basicConfig(level=logging.INFO)

client = TelegramClient('bot', API_ID, API_HASH).start(bot_token=BOT_TOKEN)
client.flood_sleep_threshold = 120

print_log = {}

settings = {}

def set_setting(user_id, key, value):
	usersettings = settings.get(user_id, {})
	usersettings[key] = value
	settings[user_id] = usersettings

def get_setting(user_id, key, default=None):
	usersettings = settings.get(user_id, {})
	return usersettings.get(key, default)

@client.on(events.NewMessage(pattern='^/id'))
async def debug_id(ev):
	await ev.respond(f"Hello! Your id is `{ev.peer_id.user_id}` please add it to the ADMIN_ID to give youself privileges :)")

@client.on(events.NewMessage(pattern='^/start'))
async def welcome(ev):
	await ev.respond(WELCOME_MSG)
	if (ev.peer_id.user_id not in print_log) and PASSWORD:
		await ev.respond(UNLOCK_MSG)

@client.on(events.NewMessage(pattern="^/reset "))
async def reset_settings(ev):
	settings[ev.peer_id.user_id] = {}

@client.on(events.NewMessage(pattern="^/font "))
async def set_font(ev):
	font = ev.message.message[6:]
	set_setting(ev.peer_id.user_id, 'font', font)

@client.on(events.NewMessage(pattern="^/size "))
async def set_size(ev):
	size = ev.message.message[6:]
	set_setting(ev.peer_id.user_id, "size", size)

# This one triggers on a single message with the pin code written
@client.on(events.NewMessage(pattern=PASSWORD, func=lambda e: e.is_private))
async def unlock_printer(ev):
	if ev.peer_id.user_id not in print_log:
		print_log[ev.peer_id.user_id] = 0
		if PASSWORD:
			await ev.respond(UNLOCKED_MSG)

@client.on(events.NewMessage(incoming=True, func=lambda e: e.is_private and e.message.message != PASSWORD))
async def handler(ev):
	if ev.message.message.startswith("/font") or ev.message.message.startswith("/size") or ev.message.message.startswith("/reset") or ev.message.message.startswith(PASSWORD):
		return # hack hack hack!!!!!
	
	msg = ev.message
	if ev.peer_id.user_id not in print_log:
		await ev.respond(UNLOCK_MSG)

	# Check if the file is valid
	if msg.photo:
		fn = join(CACHE_DIR, f"{msg.photo.id}.jpg")
	elif msg.sticker:
		fn = join(CACHE_DIR, f"{msg.sticker.id}.webp")
		for att in msg.sticker.attributes:
			if isinstance(att, DocumentAttributeAnimated):
				fn = None
				break
	elif msg.document:
		if msg.document.mime_type.startswith("image"):
			# naming it .png regardless of actual type is stupid but it works
			# works around a weird telethon filename behavior
			fn = join(CACHE_DIR, f"{msg.document.id}.png")
			print(fn)
	elif msg.message != "":
		userfont = get_setting(ev.peer_id.user_id, 'font', 'Roboto')
		usersize = get_setting(ev.peer_id.user_id, 'size', '40')
		text = msg.message
		cmd = ["convert", "-background", "white", "-fill", "black", "-font", f"{userfont}, \"Noto Color Emoji\"", "-pointsize", usersize, "-size", f"{WIDTH_PX}x", "-define", "pango:wrap=word-char", f"pango:{msg.message}", str(join(CACHE_DIR, "textimage.png"))]
		if text.startswith("/fit "):
			text = text[5:]
			cmd = ["convert", "-background", "white", "-fill", "black", "-family", userfont, "-size", f"{WIDTH_PX}x", f"label:{text}", str(join(CACHE_DIR, "textimage.png"))]
		run = subprocess.run(cmd)
		if run.returncode == 0:
			fn = join(CACHE_DIR, "textimage.png")
	else:
		fn = None
		
	if not fn:
		await ev.respond(FORMAT_ERR_MSG)
		return
	
	# Check if the user is still in the cooldown period
	time_left = int((print_log[ev.peer_id.user_id] + BASE_COOLDOWN) - time())
	if time_left > 0:
		await ev.respond(RATELIMIT_MSG.format(time_left=time_left))
		return

	# Download the file unless it's in the cache!		
	if not isfile(fn):
		await client.download_media(msg, file=fn)
	
	# Try opening the image, at least
	try:
		img = Image.open(fn)
	except Exception as e:
		print(e)
		await ev.respond(FORMAT_ERR_MSG)
		return
	
	loop = asyncio.get_running_loop()
	loop.create_task(client.forward_messages(ADMIN_ID, ev.message))

	status_code = await print_image(img)
	if status_code == 0:
		print_log[ev.peer_id.user_id] = time()
		await ev.respond(PRINT_SUCCESS_MSG)
	else:
		await ev.respond(PRINT_FAIL_MSG)
		await client.send_message(ADMIN_ID, f'Printer is not working. Process returned status code {status_code}')
	
	if PRINT_SUCCESS_COMMAND:
		system(PRINT_SUCCESS_COMMAND)

	
async def print_image(img):
	# Limit stickers ratio (so people don't print incredibly long stickers)
	#if img.size[1]/img.size[0] > MAX_ASPECT_RATIO:
	#	await ev.respond(RATIO_ERR_MSG)
	#	return

	# Remove transparency
	if img.mode == 'RGBA':
		bg_img = Image.new(img.mode, img.size, BACKGROUND_COLOR)
		img = Image.alpha_composite(bg_img, img)

	# Resize the image
	img.thumbnail([WIDTH_PX, HEIGHT_PX], resample=Image.LANCZOS, reducing_gap=None)

	# Convert to grayscale and apply a gamma of 1.8
	img = img.convert('L')
	
	if GAMMA_CORRECTION != 1:
		img = Image.eval(img, lambda x: int(255*pow((x/255),(1/GAMMA_CORRECTION))))

	img.save(IMAGE_PATH, 'PNG')

	return system(PRINT_COMMAND)

if __name__ == '__main__':
	makedirs(CACHE_DIR, exist_ok=True)
	client.run_until_disconnected()
